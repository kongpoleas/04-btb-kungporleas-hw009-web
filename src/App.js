import './App.css';
import {Container} from 'react-bootstrap'
import Menu from './components/Menu';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './components/Home';
import Account from './components/Account';
import Video from './components/Video';
import Auth from './components/Auth';
import { useState } from 'react';
import PageDetail from './components/PageDetail';
import Welcome from './components/Welcome';

function App() {
    const [items] = useState([
       {
           title: "Angkor Wat",
           description: "Content",
           image: "https://assets.archpaper.com/wp-content/uploads/2021/02/AngkorWat_Cambodia.jpg"
       },
       {
            title:"Angkor Wat",
            description: "Content",
            image: "https://lh3.googleusercontent.com/proxy/vbv7gYF4dtT3nsOMoQSnDj0_VODq_HIlVxZVcnBD-KuuWC1I17YLrFACJX4Ec31ypm49L5XkEoM7_G2Mc18hbHWDYE3u6nLLLj_poWqYSRklz27aLcq87mEm4yOsXJboXA8RKmR8MTlN_Bn7Em19etDcQJV7"
       },
       {
            title:"Preh Vihear Temple",
            description: "Content",
            image: "https://i0.wp.com/codeandtravels.com/wp-content/uploads/2018/08/SAM_5040.jpg?fit=2000%2C1333&ssl=1"
       },
       {
            title:"Preh Vihear Temple",
            description: "Content",
            image: "https://sarathte.files.wordpress.com/2011/10/preah_vihear_temple-600x2902.jpg"
       },
       {
            title:"Preh Vihear Temple",
            description: "Content",
            image: "https://whc.unesco.org/uploads/thumbs/site_1224_0017-750-0-20130711145029.jpg"
       },
       {
            title:"Preh Vihear Temple",
            description: "Content",
            image: "https://www.angkorexpressboat.com/uploads/preahvihear-temple-seladavid.jpg"
       }
    ])

  

  return (
    <BrowserRouter>
          <Route  />
    <Menu/>
        <Switch>
            <Route exact path="/">
                <Home items={items}/>
            </Route>
            <Route path="/video" component={Video}/>
            <Route path="/account" component={Account}/>
            <Route path="/auth">
               <Auth/>
            </Route>
            <Route path="/welcome">
               <Welcome/>
            </Route>
            <Route path="/detail/:id" component={PageDetail}/>
            <Route path ='*' render={()=> <Container>
             <h3> 404 page not found</h3>
           </Container>}></Route>
        </Switch>
    </BrowserRouter>
  );
}

export default App;
