import React from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

function Home(props) {
    return (
        <Container>
            <Row>
            {
                props.items.map((item, index)=>{
                    return (
                        <Col className="mt-5" md={3} key={index}>
                        <Card>
                        <Card.Img variant="top" src={item.image} height="180px"/>
                        <Card.Body>
                            <Card.Title>{item.title}</Card.Title>
                            <Card.Text>
                            {item.description}
                            </Card.Text>
                            <Link to={`/detail/${index}`}>
                                <Button variant="light" >Read</Button>
                            </Link>
                        </Card.Body>
                        </Card>
                        </Col>
                    )
                })
            }
            </Row>
        </Container>
    )
}

export default Home
