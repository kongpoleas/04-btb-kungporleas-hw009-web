import React from 'react'
import { Button, ButtonGroup, Container } from 'react-bootstrap'
import { NavLink, Route, Switch, useRouteMatch } from 'react-router-dom'
import AnimationView from '../Views/AnimationView';
import MovieView from '../Views/MovieView';

function Video() {
    let {url } = useRouteMatch();
    return (
        <Container>
            <h1>Video</h1>
            <ButtonGroup aria-label="Basic example">
                <Button as={NavLink} to={`${url}/movie`}  variant="secondary" >Movie</Button>
                <Button as={NavLink} to={`${url}/animation`} variant="secondary">Animation</Button>
            </ButtonGroup>
            <Switch>
                <Route path="/video/movie">
                    <MovieView/>
                </Route>
                <Route path="/video/animation">
                    <AnimationView/>
                </Route>
            </Switch>
        </Container>
    )
}

export default Video
