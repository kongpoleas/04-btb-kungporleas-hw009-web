import React from 'react'
import { Container } from 'react-bootstrap';
import { useParams } from 'react-router';

function PageDetail() {
    let  { id }  = useParams();
    return (
        <Container>
            <h1>Detail {id}</h1>
        </Container>
    )
}

export default PageDetail
