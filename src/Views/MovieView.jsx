import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap';
import { NavLink, useLocation, useRouteMatch } from 'react-router-dom'
import MovieSelection from './MovieSelection';

function MovieView() {
    let {url} = useRouteMatch();
    let location = useLocation();
    let query = new URLSearchParams(location.search);
    let type = query.get("type");
    return (
        <div>
            <h1>
                Movie Category
            </h1>
            <ButtonGroup aria-label="Basic example">
                <Button as={NavLink} to={`${url}?type=adventure`} variant="secondary">Adventure</Button>
                <Button as={NavLink} to={`${url}?type=crime`} variant="secondary">Crime</Button>
                <Button as={NavLink} to={`${url}?type=action`} variant="secondary">Action</Button>
                <Button as={NavLink} to={`${url}?type=romance`} variant="secondary">Romance</Button>
                <Button as={NavLink} to={`${url}?type=comedy`} variant="secondary">Comedy</Button>
            </ButtonGroup>
            <MovieSelection type={type}/>
        </div>
    )
}

export default MovieView
