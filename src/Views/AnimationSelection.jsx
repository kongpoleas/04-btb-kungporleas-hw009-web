import React from 'react'

function AnimationSelection(props) {
    return (
        <div>
            <h3>Please choose category : <span className="red-text">{props.type}</span></h3>
        </div>
    )
}

export default AnimationSelection
