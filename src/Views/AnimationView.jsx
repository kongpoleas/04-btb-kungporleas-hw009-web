import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { NavLink, useLocation, useRouteMatch } from 'react-router-dom'
import AnimationSelection from './AnimationSelection';

function AnimationView() {
    let {url } = useRouteMatch();
    let location = useLocation();
    let query = new URLSearchParams(location.search);
    let type = query.get("type");
    return (
        <div>
            <h1>
                Animation Category
            </h1>
            <ButtonGroup aria-label="Basic example">
                <Button as={NavLink} to={`${url}?type=action`} variant="secondary">Action</Button>
                <Button as={NavLink} to={`${url}?type=romance`} variant="secondary">Romance</Button>
                <Button as={NavLink} to={`${url}?type=comedy`} variant="secondary">Comedy</Button>
            </ButtonGroup>
            <AnimationSelection type={type}/>
        </div>
    )
}

export default AnimationView
